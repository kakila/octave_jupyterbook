# octave_jupyterbook

Collection of GNU Octave tutorials using jupyter notebooks.

Source code is distributed under the terms of the GNU General Public License version 3.

Other material is distributed under the terms of the Creative Commons Attribution-ShareAlike International Public License version 4.0.

## Cleaning with git and notebooks

If you commit a notebook with outputs you can be committing binary blobs, which
might cause the repository to grow unnecessarily.
to avoid this problem we have found two solutions

### Git hooks: pre-commit

This solution clean the output of your local files as well.

To put it in place, copy the file `etc/pre-commit` to your local `.git/hooks/` folder.

### Git filters

This solution does not clear the output in your local files

To put it in place, add the content of the file `etc/strip_notebook_output.filter`
to your local `.git/config` file.
This solution requires that the file `.attributes` contains the line

```
*.ipynb filter=strip-notebook-output
```

which should already be the case


